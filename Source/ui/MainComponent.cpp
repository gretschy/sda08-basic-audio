/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& a) : audio (a)

{
    setSize (500, 400);
    
    startStopButton.setButtonText("Start");
    addAndMakeVisible(startStopButton);
    startStopButton.addListener(this);
    
    waveSelector.addItem("Sine", 1);
    waveSelector.addItem("Square", 2);
    waveSelector.addItem("Saw", 3);
    waveSelector.addItem("Triangle", 4);
    waveSelector.setSelectedId(1);
    addAndMakeVisible(waveSelector);
    waveSelector.addListener(this);
    
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    startStopButton.setBounds(10, 10, getWidth() - 20, 40);
    waveSelector.setBounds(10, 100, 80, 40);
}

void MainComponent::buttonClicked(Button* button)
{    
    if (isRunning == true)
    {
        isRunning = false;
        counter.threadStart();
        startStopButton.setButtonText("Stop");
    }
    
    else if (isRunning == false)
    {
        isRunning = true;
        counter.threadStop();
        startStopButton.setButtonText("Start");
    }
}

void MainComponent::comboBoxChanged(ComboBox* combobox)
{
    
    if (waveSelector.getSelectedId() == 1)
    {
        audio.setWaveform(1);
    }
    
    else if (waveSelector.getSelectedId() == 2)
    {
        audio.setWaveform(2);
    }
    
    else if (waveSelector.getSelectedId() == 3)
    {
        audio.setWaveform(3);
    }
    
    else if (waveSelector.getSelectedId() == 4)
    {
        audio.setWaveform(4);
    }
}


//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Preferences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

