//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Tom on 12/11/2016.
//
//

#include "SinOscillator.hpp"
#include "Oscillator.hpp"
#include <cmath>

SinOscillator::SinOscillator()
{
    reset();
}

SinOscillator::~SinOscillator()
{
    
}

float SinOscillator::renderWaveShape (const float currentPhase)
{
    return sin (currentPhase);
}

