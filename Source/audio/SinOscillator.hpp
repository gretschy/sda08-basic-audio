//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Tom on 12/11/2016.
//
//

#ifndef H_SINOSCILLATOR
#define H_SINOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"

/**
 Class for a sinewave oscillator
 */

class SinOscillator : public Oscillator
{
public:
    //==============================================================================
    /**
     SinOscillator constructor
     */
    SinOscillator();
    
    /**
     SinOscillator destructor
     */
    virtual ~SinOscillator();
    
    /**
     function that provides the execution of the waveshape
     */
    virtual float renderWaveShape (const float currentPhase);
    
private:
    
};

#endif //H_SINOSCILLATOR