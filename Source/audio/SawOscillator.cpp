//
//  SawOscillator.cpp
//  JuceBasicAudio
//
//  Created by Tom on 12/11/2016.
//
//

#include "SawOscillator.hpp"

SawOscillator::SawOscillator()
{
    
}

SawOscillator::~SawOscillator()
{
    
}

float SawOscillator::renderWaveShape(const float currentPhase)
{
    float sawPosition;
    
    if (currentPhase <= M_PI)
    {
        sawPosition = 1.0;
    }
    
    else if (currentPhase >= 2* M_PI)
    {
        sawPosition = -1.0 - currentPhase;
    }
    
    return sawPosition;
}