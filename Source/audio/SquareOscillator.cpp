//
//  SquareOscillator.cpp
//  JuceBasicAudio
//
//  Created by Tom on 10/11/2016.
//
//

#include "SquareOscillator.hpp"
#include <cmath>

SquareOscillator::SquareOscillator()
{
    
}

SquareOscillator::~SquareOscillator()
{
    
}

float SquareOscillator:: renderWaveShape(const float currentPhase)
{
    float squarePosition;
    
    if (currentPhase <= M_PI)
    {
        squarePosition = 1;
    }
    
    else if (currentPhase > M_PI && currentPhase <= 2*M_PI)
    {
        squarePosition = -1;
    }
    
    return squarePosition;
}