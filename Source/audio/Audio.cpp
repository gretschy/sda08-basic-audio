/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    osc = &sineOsc;
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    
    if (message.isNoteOn())
    {
        osc.get()->setFrequency(message.getMidiNoteInHertz(message.getNoteNumber()));
        DBG("Note number: " << message.getNoteNumber() << "\n");
        
        osc.get()->setAmplitude(message.getVelocity()/127.0);
        DBG("Velocity: " << message.getVelocity()/127.0 << "\n");
    }

    else if (message.isNoteOff())
        {
            osc.get()->setAmplitude(0.0);
        }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        float wave = osc.get()->nextSample();
        
        *outL = wave;
        *outR = wave;
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("Device About to Start");
    sineOsc.setSampleRate(device->getCurrentSampleRate());
    squareOsc.setSampleRate(device->getCurrentSampleRate());
    sawOsc.setSampleRate(device->getCurrentSampleRate());
    triOsc.setSampleRate(device->getCurrentSampleRate());
}

void Audio::audioDeviceStopped()
{
    DBG("Device Stopped");
}

void Audio::setGain(float newGain)
{
    sineOsc.setAmplitude(newGain);
    squareOsc.setAmplitude(newGain);
    sawOsc.setAmplitude(newGain);
    triOsc.setAmplitude(newGain);
    
}

float Audio::getGain()
{
    float ampValue = amplitude.get();
    return ampValue;
}

void Audio::setWaveform(int wave)
{
    switch (wave)
    {
        case 1:
            osc = &sineOsc;
            DBG("Sine Wave Selected");
            break;
            
        case 2:
            osc = &squareOsc;
            DBG("Square Wave Selected");
            break;
            
        case 3:
            osc = &sawOsc;
            DBG("Saw Wave Selected");
            break;
            
        case 4:
            osc = &triOsc;
            DBG("Triangle Wave Selected");
            break;
            
        default:
            break;
    }
}