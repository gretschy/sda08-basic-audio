//
//  SquareOscillator.hpp
//  JuceBasicAudio
//
//  Created by Tom on 10/11/2016.
//
//

#ifndef SquareOscillator_hpp
#define SquareOscillator_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"

/** class for a square wave oscillator*/

class SquareOscillator : public Oscillator
{
public:
    
    /**constructor*/
    SquareOscillator();
    
    /**destructor*/
    ~SquareOscillator();
    
    /** override renderWaveShape function to define the waveshape*/
    float renderWaveShape(const float currentPhase) override;
    
private:
};

#endif /* SquareOscillator_hpp */
