//
//  TriangleOscillator.cpp
//  JuceBasicAudio
//
//  Created by Tom on 12/11/2016.
//
//

#include "TriangleOscillator.hpp"

TriangleOscillator::TriangleOscillator()
{
    
}

TriangleOscillator::~TriangleOscillator()
{
    
}

float TriangleOscillator::renderWaveShape(const float currentPhase)
{
    float trianglePositiion;
    
    if (currentPhase <= M_PI)
    {
        trianglePositiion = 1.0 - currentPhase;
    }
    
    else if (currentPhase >= 2* M_PI)
    {
        trianglePositiion = -1.0 - currentPhase;
    }
    
    return trianglePositiion;
}