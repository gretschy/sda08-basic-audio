//
//  TriangleOscillator.hpp
//  JuceBasicAudio
//
//  Created by Tom on 12/11/2016.
//
//

#ifndef TriangleOscillator_hpp
#define TriangleOscillator_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"


class TriangleOscillator : public Oscillator
{
public:
    
    /**constructor*/
    TriangleOscillator();
    
    /**destructor*/
    ~TriangleOscillator();
    
    /** override renderWaveShape function to define the waveshape*/
    float renderWaveShape(const float currentPhase) override;
    
private:
};


#endif /* TriangleOscillator_hpp */
