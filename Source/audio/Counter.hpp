//
//  Counter.hpp
//  JuceBasicAudio
//
//  Created by Tom on 10/11/2016.
//
//

#ifndef Counter_hpp
#define Counter_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"


class Counter : public Thread
{
public:
    
    Counter();
    ~Counter();
    
    void run() override;
    void threadStart();
    void threadStop();
    
    
private:
    
};

#endif /* Counter_hpp */
