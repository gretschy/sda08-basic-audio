//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Tom on 10/11/2016.
//
//

#include "Counter.hpp"


Counter::Counter() : Thread("CounterThread")
{
    
}

Counter::~Counter()
{
    stopThread(500);
    
}

void Counter::run()
{
    const int countingStartPoint = Time::getMillisecondCounter();
    
    while (!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        uint32 count = Time::getMillisecondCounter() - countingStartPoint;
        
        std::cout << "Counter:" << count << "\n";
        Time::waitForMillisecondCounter (time + 100);
    }
}

void Counter::threadStart()
{
    startThread();
}

void Counter::threadStop()
{
    stopThread(500);
}