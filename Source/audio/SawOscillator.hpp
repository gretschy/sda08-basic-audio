//
//  SawOscillator.hpp
//  JuceBasicAudio
//
//  Created by Tom on 12/11/2016.
//
//

#ifndef SawOscillator_hpp
#define SawOscillator_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"

class SawOscillator : public Oscillator
{
public:
    
    /**constructor*/
    SawOscillator();
    
    /**destructor*/
    ~SawOscillator();
    
    /** override renderWaveShape function to define the waveshape*/
    float renderWaveShape(const float currentPhase) override;
    
private:
    
};

#endif /* SawOscillator_hpp */
